from django.shortcuts import render
from media.models import Media, MediaType
from media.serializers import MediaSerializer, MediaTypeSerializer

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
from rest_framework.decorators import api_view


@api_view(['GET', 'POST'])
def media_list(request):
    # This method will manage GET and POST requests to list and insert medias
    if request.method == 'GET':
        medias = Media.objects.all()
        
        title = request.GET.get('title', None)
        if title is not None:
            medias = medias.filter(title__icontains=title)
        
        media_serializer = MediaSerializer(medias, many=True)
        return JsonResponse(media_serializer.data, safe=False)

    elif request.method == 'POST':
        media_data = JSONParser().parse(request)
        media_serializer = MediaSerializer(data=media_data)

        if media_serializer.is_valid():
            media_serializer.save()
            return JsonResponse(media_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(media_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def media_detail(request, pk):
    # find tutorial by pk (id)
    try: 
        media = Media.objects.get(pk=pk)

        if request.method == 'GET': 
            media_serializer = MediaSerializer(media) 
            return JsonResponse(media_serializer.data) 

        elif request.method == 'PUT': 
            media_data = JSONParser().parse(request) 
            media_serializer = MediaSerializer(media, data=media_data) 

            if media_serializer.is_valid(): 
                media_serializer.save() 
                return JsonResponse(media_serializer.data) 
            return JsonResponse(media_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
        elif request.method == 'DELETE': 
            media.delete() 
            return JsonResponse({'message': 'La media fue eliminada con exito!'}, status=status.HTTP_204_NO_CONTENT)
    except Media.DoesNotExist: 
        return JsonResponse({'message': 'La entidad Media no existe'}, status=status.HTTP_404_NOT_FOUND) 
 

@api_view(['GET', 'POST'])
def media_type_list(request):

    if request.method == 'GET':
        medias = Media.Type.objects.all()
        
        media_type_serializer = MediaTypeSerializer(medias, many=True)
        return JsonResponse(media_type_serializer.data, safe=False)

    elif request.method == 'POST':
        media_type_data = JSONParser().parse(request)
        media_type_serializer = MediaTypeSerializer(data=media_type_data)

        if media_type_serializer.is_valid():
            media_type_serializer.save()
            return JsonResponse(media_type_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(media_type_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
