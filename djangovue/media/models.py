from django.db import models

    
class MediaType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, blank=False)

    def __str__(self):
        return self.name


class Media(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=250, blank=False)
    url = models.CharField(max_length=350, blank=True)
    description = models.TextField(blank=True)
    director = models.CharField(max_length=250, blank=True)
    producer = models.CharField(max_length=250, blank=True)
    writer = models.CharField(max_length=250, blank=True)
    duration = models.FloatField(blank=True)
    release_year = models.IntegerField(blank=True)
    video_url = models.CharField(max_length=350, blank=True)
    
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update_date = models.DateTimeField(auto_now=True)

    type = models.OneToOneField(
        MediaType,
        on_delete = models.CASCADE,
        blank=True
    )

    def __str__(self):
        return self.title




