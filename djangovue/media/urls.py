
from django.urls import include, path
from media import views 
 
urlpatterns = [ 
    path(r'api/media', views.media_list),
    path(r'api/media_type', views.media_type_list),
    path(r'api/media/(?P<pk>[0-9]+)$', views.media_detail),
    # url(r'^api/media/published$', views.tutorial_list_published)
]
