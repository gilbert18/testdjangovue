from rest_framework import serializers
from .models import Media, MediaType

class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media
        fields = ['title','url','description','director', 'producer', 'writer', 'duration', 
            'release_year', 'video_url', 'creation_date', 'last_update_date', 'type']



class MediaTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaType
        fields = ['name']