# Generated by Django 4.0.1 on 2022-02-02 03:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='media',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='media',
            name='director',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='media',
            name='duration',
            field=models.FloatField(blank=True),
        ),
        migrations.AlterField(
            model_name='media',
            name='producer',
            field=models.CharField(blank=True, max_length=250),
        ),
        migrations.AlterField(
            model_name='media',
            name='release_year',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='media',
            name='type',
            field=models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, to='media.mediatype'),
        ),
        migrations.AlterField(
            model_name='media',
            name='url',
            field=models.CharField(blank=True, max_length=350),
        ),
        migrations.AlterField(
            model_name='media',
            name='video_url',
            field=models.CharField(blank=True, max_length=350),
        ),
        migrations.AlterField(
            model_name='media',
            name='writer',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
